import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-manifest',
  templateUrl: './manifest.component.html',
  styleUrls: ['./manifest.component.scss']
})
export class ManifestComponent implements OnInit {

  commercialAir = [
    {value: '0', viewValue: 'Commercial Air'},
    {value: '1', viewValue: 'Commercial Vessel'},
    {value: '2', viewValue: 'Private Air'},
    {value: '3', viewValue: 'Bus '},
    {value: '4', viewValue: 'Rail'}
  ];


  constructor() { }

  ngOnInit() {
  }

}
