import { Component, OnInit } from '@angular/core';

interface Car {
  vin: string;
  color: string;
  year: string;
  brand: string;
}

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  datasource: Car[] = [
    {vin: 'vin', color: 'red', year: '2018', brand: 'Camry'},
    {vin: 'vin', color: 'red', year: '2018', brand: 'Camry'},
    {vin: 'vin', color: 'red', year: '2018', brand: 'Camry'},
    {vin: 'vin', color: 'red', year: '2018', brand: 'Camry'},
    {vin: 'vin', color: 'red', year: '2018', brand: 'Camry'}
  ];
  cars: Car[];
  totalRecords: number;
  cols: any[];

  loading: boolean;


  ngOnInit() {
    //datasource imitation
    this.cols = [
      { field: 'vin', header: 'Vin' },
      { field: 'year', header: 'Year' },
      { field: 'brand', header: 'Brand' },
      { field: 'color', header: 'Color' }
    ];

    this.loading = true;
  }

  loadCarsLazy(event) {
    this.loading = true;

    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    //imitate db connection over a network
    setTimeout(() => {
      if (this.datasource) {
        this.cars = this.datasource.slice(event.first, (event.first + event.rows));
        this.loading = false;
      }
    }, 1000);
  }

/*  constructor() { }

  ngOnInit() {
  }*/

}
