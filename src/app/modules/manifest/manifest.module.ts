import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ManifestRoutingModule } from './manifest-routing.module';
import { ManifestComponent } from './components/manifest/manifest.component';
import {MatFormFieldModule, MatSelectModule, MatIconModule, MatInputModule,MatButtonModule,MatMenuModule} from '@angular/material';
import { SearchResultComponent } from './components/search-result/search-result.component';
import {AccordionModule} from 'primeng/accordion';
import {TableModule} from 'primeng/table';

@NgModule({
  imports: [
    CommonModule,
    ManifestRoutingModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    TableModule
  ],

  declarations: [
    ManifestComponent,
    SearchResultComponent
  ]
})



export class ManifestModule  {
}
