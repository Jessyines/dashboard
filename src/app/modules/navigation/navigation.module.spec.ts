import { NavigationModule } from './navigation.module';

describe('ManifestModule', () => {
  let navigationModule: NavigationModule;

  beforeEach(() => {
    navigationModule = new NavigationModule();
  });

  it('should create an instance', () => {
    expect(navigationModule).toBeTruthy();
  });
});
